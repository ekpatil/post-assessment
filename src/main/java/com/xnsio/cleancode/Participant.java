package com.xnsio.cleancode;

import java.time.LocalDate;
import java.util.*;

public class Participant {

    private String name;
    private List<DaySchedule> schedules;

    public Participant(String name) {
        this.name = name;
        schedules = new ArrayList<>();
    }

    public void addSchedule(DaySchedule daySchedule){
        schedules.add(daySchedule);
    }

    public List<Integer> getFreeSlotsByDay(LocalDate day){
        Optional<DaySchedule> schedule = schedules.stream().filter(daySchedule -> daySchedule.isEqual(day)).findFirst();
        if(!schedule.isPresent()){
            return Collections.EMPTY_LIST;
        }
        return schedule.get().getFreeMeetingSlots();
    }
}
