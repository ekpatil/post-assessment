package com.xnsio.cleancode;


import java.time.LocalDate;
import java.util.*;

public class Meeting
{
    public static List<Integer> MEETING_SLOTS = Arrays.asList(8,9,10,11,12,13,14,15,16);

    public Map<LocalDate, Integer> scheduleMeeting(List<Participant> participants, LocalDate lastDate, Integer lastSlot){
        LocalDate meetingDate = LocalDate.now();
        Map<LocalDate, Integer> commonSlot = new HashMap<>();
        while(meetingDate.isBefore(lastDate) || meetingDate.isEqual(lastDate)) {
            List<Integer> availableSlots = new ArrayList<>(MEETING_SLOTS);
            for (Participant participant: participants) {
                availableSlots.retainAll(participant.getFreeSlotsByDay(meetingDate));
            }
            if(availableSlots.size() > 0 && availableSlots.get(0) <= lastSlot){
                commonSlot.put(meetingDate,availableSlots.get(0));
                return commonSlot;
            }
            meetingDate = meetingDate.plusDays(1);
        }
        return commonSlot;
    }
}
