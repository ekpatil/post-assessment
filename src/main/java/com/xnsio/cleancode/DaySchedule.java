package com.xnsio.cleancode;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.xnsio.cleancode.Meeting.MEETING_SLOTS;

public class DaySchedule {


    private Map<Integer,String> meetings ;
    private LocalDate day;

    public DaySchedule(LocalDate day) {
        this.day = day;
        meetings = new TreeMap<>();

    }

    public void addMeeting(Integer slot, String desc) {
        meetings.put(slot, desc);
    }

    public boolean isEqual(LocalDate day) {
        return this.day.isEqual(day);
    }

    public List<Integer> getFreeMeetingSlots(){
        List<Integer> slots = new ArrayList<>(MEETING_SLOTS);
        slots.removeAll(meetings.keySet());
        return slots;
    }
}
