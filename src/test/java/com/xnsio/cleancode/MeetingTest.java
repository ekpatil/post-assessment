package com.xnsio.cleancode;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.IntStream;

public class MeetingTest
{
    private Meeting meeting = new Meeting();
    private Map<String, Participant> sampleCalendars = new HashMap<>();

    @Before
    public void setup(){
        sampleCalendars.put("Mini",addParticipantMini());
        sampleCalendars.put("Brad",addParticipantBrad());
        sampleCalendars.put("Janet",addParticipantJanet());
        sampleCalendars.put("Frank",addParticipantFrank());
        sampleCalendars.put("BusyMan",addParticipantBusyMan());
    }


    @Test
    public void scheduleSingleParticipantMeeting() {
        Map<LocalDate, Integer> availableSlot = meeting.scheduleMeeting(getSingleParticipant("Mini"), LocalDate.now().plusDays(1), 16);
        assertThat(availableSlot.size(), is(1));
        assertThat(availableSlot.get(LocalDate.now()), is(11));
    }

    @Test
    public void scheduleMeetingWithTwoParticipants() {
        Map<LocalDate, Integer> availableSlot = meeting.scheduleMeeting(getSingleParticipant("Mini","Frank"), LocalDate.now().plusDays(1), 16);
        assertThat(availableSlot.size(), is(1));
        assertThat(availableSlot.get(LocalDate.now().plusDays(1)), is(10));
    }

    @Test
    public void scheduleMeetingWithMultipleParticipants() {
        Map<LocalDate, Integer> availableSlot = meeting.scheduleMeeting(getSingleParticipant("Mini","Frank","Brad", "Janet"), LocalDate.now().plusDays(1), 16);
        assertThat(availableSlot.size(), is(1));
        assertThat(availableSlot.get(LocalDate.now().plusDays(1)), is(14));
    }

    @Test
    public void noSlotsAvailable() {
        Map<LocalDate, Integer> availableSlot = meeting.scheduleMeeting(getSingleParticipant("BusyMan"), LocalDate.now().plusDays(1), 16);
        assertThat(availableSlot.size(), is(0));
    }

    @Test
    public void noSlotAvailableTodayBefore10AM() {
        Map<LocalDate, Integer> availableSlot = meeting.scheduleMeeting(getSingleParticipant("Mini"), LocalDate.now(), 10);
        assertThat(availableSlot.size(), is(0));
    }

    private List<Participant> getSingleParticipant(String...participants) {
        List<Participant> participantList = new ArrayList<>();
        for(String participant : participants){
            participantList.add(sampleCalendars.get(participant));
        }
        return participantList;
    }


    private Participant addParticipantBusyMan() {
        Participant busyMan = new Participant("BusyMan");

        DaySchedule firstDay = new DaySchedule(LocalDate.now());
        IntStream.rangeClosed(8,16).forEach(i->firstDay.addMeeting(i,"busy"));
        busyMan.addSchedule(firstDay);

        DaySchedule secondDay = new DaySchedule(LocalDate.now().plusDays(1));
        IntStream.rangeClosed(8,16).forEach(i->secondDay.addMeeting(i,"busy"));
        busyMan.addSchedule(secondDay);

        return busyMan;
    }


    private Participant addParticipantFrank() {
        Participant frank = new Participant("Frank");

        DaySchedule firstDay = new DaySchedule(LocalDate.now());
        firstDay.addMeeting(10,"Team Planning");
        firstDay.addMeeting(11,"Lunch");
        firstDay.addMeeting(12,"Busy");
        firstDay.addMeeting(13,"Meet Brad");
        firstDay.addMeeting(14,"Busy");
        firstDay.addMeeting(15,"Busy");
        firstDay.addMeeting(16,"Busy");
        frank.addSchedule(firstDay);

        DaySchedule secondDay = new DaySchedule(LocalDate.now().plusDays(1));
        secondDay.addMeeting(8,"Meet Brad");
        secondDay.addMeeting(9,"Meet Janet");
        secondDay.addMeeting(11,"Lunch");
        secondDay.addMeeting(15,"Meet Janet");
        secondDay.addMeeting(16,"Busy");
        frank.addSchedule(secondDay);

        return frank;
    }

    private Participant addParticipantJanet() {
        Participant janet = new Participant("Janet");

        DaySchedule firstDay = new DaySchedule(LocalDate.now());
        firstDay.addMeeting(9,"Meet mini");
        firstDay.addMeeting(10,"Team Planning");
        firstDay.addMeeting(12,"Lunch");
        firstDay.addMeeting(13,"Busy");
        firstDay.addMeeting(14,"Busy");
        janet.addSchedule(firstDay);

        DaySchedule secondDay = new DaySchedule(LocalDate.now().plusDays(1));
        secondDay.addMeeting(8,"Meet Mini");
        secondDay.addMeeting(9,"Meet Frank");
        secondDay.addMeeting(10,"Busy");
        secondDay.addMeeting(12,"Lunch");
        secondDay.addMeeting(13,"Busy");
        secondDay.addMeeting(15,"Meet Frank");
        janet.addSchedule(secondDay);

        return janet;
    }

    private Participant addParticipantBrad() {
        Participant brad = new Participant("Brad");

        DaySchedule firstDay = new DaySchedule(LocalDate.now());
        firstDay.addMeeting(8,"Meet Mini");
        firstDay.addMeeting(9,"Meet Frank");
        firstDay.addMeeting(10,"Team Planning");
        firstDay.addMeeting(12,"Lunch");
        firstDay.addMeeting(13,"Meet Frank");
        firstDay.addMeeting(14,"Busy");
        firstDay.addMeeting(15,"Busy");
        firstDay.addMeeting(16,"Busy");
        brad.addSchedule(firstDay);

        DaySchedule secondDay = new DaySchedule(LocalDate.now().plusDays(1));
        secondDay.addMeeting(8,"Meet Frank");
        secondDay.addMeeting(9,"Meet Mini");
        secondDay.addMeeting(12,"Lunch");
        brad.addSchedule(secondDay);

        return brad;
    }

    private Participant addParticipantMini() {
        Participant mini = new Participant("Mini");

        DaySchedule firstDay = new DaySchedule(LocalDate.now());
        firstDay.addMeeting(8,"Meet Brad");
        firstDay.addMeeting(9,"Meet Janet");
        firstDay.addMeeting(10,"Team Planning");
        firstDay.addMeeting(12,"Lunch");
        firstDay.addMeeting(14,"Busy");
        firstDay.addMeeting(15,"Busy");
        firstDay.addMeeting(16,"Busy");
        mini.addSchedule(firstDay);

        DaySchedule secondDay = new DaySchedule(LocalDate.now().plusDays(1));
        secondDay.addMeeting(8,"Meet Janet");
        secondDay.addMeeting(9,"Meet Brad");
        secondDay.addMeeting(12,"Lunch");
        secondDay.addMeeting(15,"Busy");
        secondDay.addMeeting(16,"Busy");
        mini.addSchedule(secondDay);

        return mini;
    }
}
